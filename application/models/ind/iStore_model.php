<?php

class iStore_model extends CI_Model {

    protected $IND_Table_Store = 'ind_store';
    protected $IND_Table_User = 'ind_user';
    protected $IND_Table_Store_MainMenu = 'ind_store_main_menu';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getStoreInfo($domain = DOMAIN) {
        $cacheId = __CLASS__ . '_' . __FUNCTION__ . '_' . 'Domain' . '_' . $domain;
        $result = $this->cache->get($cacheId);
        if ($result === FALSE) {
            $this->load->database();
            $this->db->select('*');
            $this->db->where('Domain', $domain);
            $result = $this->db->get($this->IND_Table_Store)->row();
            // Save into the cache
            $this->cache->save($cacheId, $result, _CACHE_TIME);
        }
        return $result;
    }

    function getUserInfo($uid = 1) {
        $cacheId = __CLASS__ . '_' . __FUNCTION__ . '_' . 'UserID' . '_' . $uid;
        $result = $this->cache->get($cacheId);
        if ($result === FALSE) {
            $this->load->database();
            $this->db->select('*');
            $this->db->where('ID', $uid);
            $result = $this->db->get($this->IND_Table_User)->row();
            // Save into the cache
            $this->cache->save($cacheId, $result, _CACHE_TIME);
        }
        return $result;
    }

    function getUserMainMenu($uid = 1) {
        $cacheId = __CLASS__ . '_' . __FUNCTION__ . '_' . 'UserID' . '_' . $uid;
        $result = $this->cache->get($cacheId);
        if ($result === FALSE) {
            $this->load->database();
            $this->db->select('*');
            $this->db->where('UserID', $uid);
            $result = $this->db->get($this->IND_Table_Store_MainMenu)->result();
            // Save into the cache
            $this->cache->save($cacheId, $result, _CACHE_TIME);
        }return $result;
    }

}
