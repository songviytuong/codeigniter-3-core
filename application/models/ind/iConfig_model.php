<?php

class iConfig_model extends CI_Model {

    protected $IND_Table = 'ind_config';

    public function __construct() {
        parent::__construct();
    }

    function getConfig($orderby = 'Keyword', $sort = 'ASC') {
        $cacheId = __CLASS__ . '_' . __FUNCTION__ . '_' . $orderby . '_' . $sort;
        $result = $this->cache->get($cacheId);
        if ($result === FALSE) {
            $this->load->database();
            $this->db->select('Keyword,Value');
            $this->db->order_by($orderby, $sort);
            $result = $this->db->get($this->IND_Table)->result_array();
            // Save into the cache
            $this->cache->save($cacheId, $result, _CACHE_TIME);
        }

        $res = array();
        if (!empty($result) && is_array($result)) {
            foreach ($result as $key => $item) {
                $res[$key]['Keyword'] = $item['Keyword'];
                if (strtoupper($item['Value']) === "TRUE") {
                    $res[$key]['Value'] = TRUE;
                } else if (strtoupper($item['Value']) === "FALSE") {
                    $res[$key]['Value'] = FALSE;
                } else {
                    $res[$key]['Value'] = trim($item['Value']);
                }
            }
        }
        return $res;
    }

}
