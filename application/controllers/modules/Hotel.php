<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Hotel extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->template->set_template('hotel');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        $data['page_class'] = "js no-touch-mobile";
        $data['js_to_load'] = array();

//        if (!empty($products)) {
//            SEOPlugin::setTitle($products['Title']);
//            SEOPlugin::setDescription(($products['Description']) ? $products['Description'] : $products['MetaDescription']);
//            SEOPlugin::setSocialImage($products['PrimaryImage']);
//        }

        $this->template->write_view('content', '/modules/' . __CLASS__ . '/index', $data);
        $this->template->render();
    }

}
