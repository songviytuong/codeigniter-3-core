<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends MY_Controller {
    
    public $domain;
    public $subdomain;

    public function __construct() {
        parent::__construct();
        
        $this->domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
        $this->subdomain = explode('.', $this->domain);
        if (count($this->subdomain) == 3) {
            $this->session->set_userdata('subdomain', $this->subdomain[0]);
        } else {
            $this->session->set_userdata('domain', $this->domain);
        }
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        #BEGIN: DEFINE LANGUAGE
        $data["LangID"] = $this->cur_langid;
        $data["LangAlias"] = $this->cur_lang_local;
        
        #SHOW STORE
        if (count($this->subdomain) >= 3) {
            $domain = $this->session->userdata('subdomain');
            $domain = $domain . "." . DOMAIN;
        } else {
            $domain = $this->session->userdata('domain');
        }
        #GET STORE INFO
        $this->load->model("ind/iStore_model","iStore");
        $store = $this->iStore->getStoreInfo($domain);
        if($store){
            $data["StoreName"]  = unserialize($store->StoreName);
            $data["Slogan"]     = unserialize($store->Slogan);
            $data["Intro"]      = unserialize($store->Intro);
        }

        #GET TEMPLATES INFO
        $this->load->model("ind/iTemplates_model", "iTemplates");
        $temp = $this->iTemplates->getTemplateInfo($store->TempID);
        $data["tempGlobal"] = $temp;

        #DEFINE SRC TEMPLATE
        $data["TEMPLATE_PATH"] = strtolower(__CLASS__) . '/' . $temp->TempName;
        $data["TEMPLATE_SRC"] = DYNAMIC_TEMPLATE_SRC . $temp->TempName;

        $data["LOGO_SRC"] = PUBLIC_MEDIA_SRC."user".$store->UserID."/".$store->LogoSrc;
        $data["SITE_NAME"] = 'Title 1';
        $data["STORE_NAME"] = 'Store Name 1';
        $data["META_KEYWORDS"] = 'Store 1 Keywords';
        $data["META_DESCRIPTION"] = 'Store 1 Description';
        
        #BEGIN: LOAD MAIN MENU
        $mainMenu = $this->iStore->getUserMainMenu($store->UserID);
        $data["MainMenu"] = $mainMenu;

        $data['page_class'] = "js no-touch-mobile";
        $data['js_to_load'] = array();

//        if (!empty($products)) {
//            SEOPlugin::setTitle($products['Title']);
//            SEOPlugin::setDescription(($products['Description']) ? $products['Description'] : $products['MetaDescription']);
//            SEOPlugin::setSocialImage($products['PrimaryImage']);
//        }
        
        #SHOW SOCIAL
        $this->load->model("ind/iSocial_model","iSocial");
        $social = $this->iSocial->getSocialList();
        $data["social"] = $social;
        
        #SOCIAL DATA
        $social_active_data = $this->iSocial->getSocialList(1);
        $data["social_active_data"] = $social_active_data;
        
        $current = str_replace(PAGE_EXTENSION, "", uri_string());
        $data["menu_active"] = ($current) ? $current : 'index';
        switch ($current) {
            case 'index':
                $data["SITE_NAME"] = "Trang chủ";
                $this->load->view('store/' . $temp->TempName . '/templates', $data);
                break;
            case 'notFound':
                $this->load->view('store/' . $temp->TempName . '/templates_404', $data);
                break;
            default:
                $this->load->view('store/' . $temp->TempName . '/templates', $data);
                break;
        }
    }

}
