<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Lee Peace
 * Email: songviytuong@gmail.com
 * 29-10-2017
 */

class SEOPlugin {

    static $_title = "__seo_title";
    static $_description = "__seo_description";
    static $_keywords = "__seo_keywords";
    static $_social_image = "";
    static $_base_url = "";
    protected $CI;

    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->CI = & get_instance();
        $this->CI->load->model('ind/iSeo_model', 'iSeo');
        $data = $this->CI->iSeo->defineSEODefault();

        $this->setTitle($data["MetaTitle"]);
        $this->setKeywords($data["MetaKeywords"]);
        $this->setDescription($data["MetaDescription"]);
        $this->setSocialImage($data["MetaPicture"]);

        $base_url = "<base href=" . ROOT_URL . ">";
        $this->setBaseUrl($base_url);
    }

    public static function setBaseUrl($base_url) {
        self::$_base_url = $base_url;
    }

    public static function getBaseUrl() {
        return self::$_base_url;
    }

    public static function setSocialImage($src) {
        $img = UPLOAD_URL . '/' . $src;
        self::$_social_image = "<meta property='og:image' content='".$img."' />";
    }

    public static function getSocialImage() {
        return self::$_social_image;
    }

    public static function setTitle($title) {
        self::$_title = $title;
    }

    public static function getTitle() {
        return self::$_title;
    }

    public static function setKeywords($keywords) {
        self::$_keywords = $keywords;
    }

    public static function getKeywords() {
        return self::$_keywords;
    }

    public static function setDescription($description) {
        self::$_description = $description;
    }

    public static function getDescription() {
        return self::$_description;
    }

}
