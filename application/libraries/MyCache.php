<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MyCache {

    protected $CI;
    protected $CacheType;

    public function __construct() {
        $CI = & get_instance();
        $CacheType = $CI->config->item('_CACHE_TYPE');
        $CI->load->driver('cache', array('adapter' => $CacheType, 'backup' => 'file', 'key_prefix' => 'cache@'));
    }

}
