<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $limit = 20;
    public $upload_to = '';
    public $seo_default = TRUE;
    public $cur_lang = 'english';
    public $cur_langid = 1;
    public $cur_local = 'en';
    public $cur_lang_local = 'en_US';

    public function __construct() {
        parent::__construct();

        $this->load->model('ind/iConfig_model', 'iConfig');
        $app_conf = $this->iConfig->getConfig();
        foreach ($app_conf as $row) {
            define("{$row['Keyword']}", $row['Value']);
        }
        
        if (_MAINTENANCE) {
            $this->load->view('maintenance');
        }

        # SET DEFAULT TEMPLATE
        $this->template->set_template('default');

        # SET THEME SETUP FORM DATABASE
        #if(defined('_THEME')){
        #	$theme = strtolower(_THEME);
        #	if(file_exists(APPPATH.'views/themes/'.$theme.EXT)){
        #		$this->template->set_template($theme);
        #	}
        #}
        # BEGIN: LOAD LANGUAGE
        $this->load->model('ind/iLanguages_model');
        $langs = $this->session->userdata('languageTT');
        $getLang = (isset($_GET['lang'])) ? trim($_GET['lang']) : '';
        if ($getLang != "") {
            $langrow = $this->iLanguages_model->getLangNameByLocal($getLang);
            $this->session->set_userdata('languageTT', $langrow['LangName']);
        } else {
            $langsite = $this->config->item('language');
            $langrow = $this->iLanguages_model->getLanguageBySession($langsite);
            $this->session->set_userdata('languageTT', $langsite);
        }

        $this->cur_lang = $langrow['LangName'];
        $this->cur_langid = $langrow['LangID'];
        $this->cur_local = $langrow['LangLocal'];
        $this->cur_lang_local = $langrow['LangAlias'];

        $LangData = $this->iLanguages_model->defineAllLanguage();
        $ActiveLang = $this->iLanguages_model->getLanguageBySession($this->cur_lang);

        # LOAD LANG IN DB
        $this->lang->load('general', 'globals');

        # Header data
        $data_header = array(
        );

        # Footer data
        $data_footer = array(
        );

        $this->template->write_view('header', 'common/iHeader', $data_header);
        $this->template->write_view('footer', 'common/iFooter', $data_footer);
    }

}
