<?php
defined('BASEPATH') OR exit('No direct script access allowed');

###### default ####
$template['default']['template'] = 'themes/india_templates';
$template['default']['regions'] = array('header','content','footer');
$template['default']['parser'] = 'parser';
$template['default']['parser_method'] = 'parse';
$template['default']['parse_template'] = FALSE;

###### hotel ####
$template['hotel']['template'] = 'themes/hotel_templates';
$template['hotel']['regions'] = array('header','content','footer');
$template['hotel']['parser'] = 'parser';
$template['hotel']['parser_method'] = 'parse';
$template['hotel']['parse_template'] = FALSE;

$config['theme'] = $template;

